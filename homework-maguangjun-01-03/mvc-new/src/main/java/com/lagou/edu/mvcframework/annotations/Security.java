package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

/**
 * 自定义权限注解
 * 入参数组形式
 *
 * @auther: guangjun.ma
 * @date: 2020-4-21 10:39
 * @version: 1.0
 */
@Documented
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {
    String[] value() default "";
}
