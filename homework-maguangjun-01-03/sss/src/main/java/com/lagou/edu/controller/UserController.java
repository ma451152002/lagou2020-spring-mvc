package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户控制，如登录功能
 * @auther: guangjun.ma
 * @date: 2020-4-22 14:16
 * @version: 1.0
 */
@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * 登录页面
     * http://localhost:8080/user/goLogin
     * @return
     * @throws Exception
     * @auther: guangjun.ma
     * @date: 2020-4-22 14:19
     * @version: 1.0
     */
    @RequestMapping("/goLogin")
    public ModelAndView goLogin() throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    /**
     * 登录
     * @param username
     * @param password
     * @return
     * @throws Exception
     * @auther: guangjun.ma
     * @date: 2020-4-22 14:24
     * @version: 1.0
     */
    @RequestMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response, String username, String password) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        if ("root".equals(username) && "root".equals(password)){
            request.getSession().setAttribute("root","root");
            return "redirect:/resume/queryAll";//登录成功后，跳转到列表页面
        }else{
            return "login";//登录失败后，跳转到登录页面
        }
    }
}
