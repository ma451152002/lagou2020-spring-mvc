package com.lagou.edu.service.impl;

import com.lagou.edu.dao.ResumeDao;
import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ResumeServiceImpl implements ResumeService {

    @Autowired
    private ResumeDao resumeDao;

    @Override
    public List<Resume> queryAccountList() throws Exception {
        return resumeDao.findAll();
    }

    /**
     * 根据id删除
     * @param id
     * @auther: guangjun.ma
     * @date: 2020-4-22 10:10
     * @version: 1.0
     */
    @Override
    public void deleteById(Long id) {
         resumeDao.deleteById(id);
    }

    /**
     * 更新
     * @param resume
     * @return
     * @auther: guangjun.ma
     * @date: 2020-4-22 10:46
     * @version: 1.0
     */
    @Override
    public Resume saveOrUpdate(Resume resume) {
        return resumeDao.saveAndFlush(resume);
    }

    /**
     * 通过id查询
     * @param id
     * @return
     * @auther: guangjun.ma
     * @date: 2020-4-22 11:29
     * @version: 1.0
     */
    @Override
    public Resume queryById(Long id) {
        Optional<Resume> resumeOptional = resumeDao.findById(id);
        return  resumeOptional.get();
    }
}
