package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 简历管理
 * @auther: guangjun.ma
 * @date: 2020-4-22 16:23
 * @version: 1.0
 */
@Controller
@RequestMapping("/resume")
public class ResumeController {

    @Autowired
    private ResumeService resumeService;

    /**
     * 列表展示
     * @return
     * @throws Exception
     * @auther: guangjun.ma
     * @date: 2020-4-22 16:17
     * @version: 1.0
     */
    @RequestMapping("/queryAll")
    public ModelAndView  queryAll() throws Exception {
        List<Resume>  resumeList = resumeService.queryAccountList();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("resumeList",resumeList);
        modelAndView.setViewName("resume");
        return modelAndView;
    }

    /**
     * 通过id查询
     * @param id
     * @return
     * @throws Exception
     * @auther: guangjun.ma
     * @date: 2020-4-22 16:22
     * @version: 1.0
     */
    @RequestMapping("/queryById")
    public ModelAndView  queryById(long id) throws Exception {
        Resume  resume = resumeService.queryById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("resume",resume);
        modelAndView.setViewName("resume_Modify");
        return modelAndView;
    }

    /**
     * 更新
     * @param resume
     * @return
     * @throws Exception
     * @auther: guangjun.ma
     * @date: 2020-4-22 16:22
     * @version: 1.0
     */
    @RequestMapping("/modify")
    public String  modify(Resume resume) throws Exception {
        resumeService.saveOrUpdate(resume);
        return  "redirect:queryAll";
    }


    /**
     * 删除
     * @param id
     * @return
     * @throws Exception、
     * @auther: guangjun.ma
     * @date: 2020-4-22 3:11
     * @version: 1.0
     */
    @RequestMapping("/deleteById")
    public String  deleteById(Long id) throws Exception {
        resumeService.deleteById(id);
        return "redirect:queryAll";
    }
}
