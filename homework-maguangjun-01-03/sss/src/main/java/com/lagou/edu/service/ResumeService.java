package com.lagou.edu.service;

import com.lagou.edu.pojo.Resume;

import java.util.List;
import java.util.Optional;

public interface ResumeService {
    List<Resume> queryAccountList() throws Exception;

    /**
     * 根据id删除
     * @param id
     * @auther: guangjun.ma
     * @date: 2020-4-22 10:10
     * @version: 1.0
     */
    void deleteById(Long id);

    /**
     * 更新
     * @param resume
     * @return
     * @auther: guangjun.ma
     * @date: 2020-4-22 10:46
     * @version: 1.0
     */
    public Resume saveOrUpdate(Resume resume);

    /**
     * 通过id查询
     * @param id
     * @return
     * @auther: guangjun.ma
     * @date: 2020-4-22 11:29
     * @version: 1.0
     */
    public Resume queryById(Long id);


}
