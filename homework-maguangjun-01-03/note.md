---

---

\----------

# 高薪训练营作业---第一阶段-第③模块

**作者：马光君**

[TOC]



### 作业一：

#### 1.需求

手写MVC框架基础上增加如下功能

1）定义注解@Security（有value属性，接收String数组），该注解用于添加在Controller类或者Handler方法上，表明哪些用户拥有访问该Handler方法的权限（注解配置用户名）

2）访问Handler时，用户名直接以参数名username紧跟在请求的url后面即可，比如http://localhost:8080/demo/handle01?username=zhangsan

3）程序要进行验证，有访问权限则放行，没有访问权限在页面上输出

注意：自己造几个用户以及url，上交作业时，文档提供哪个用户有哪个url的访问权限

#### 2.需求分析

2.1基于自定义项目mvc开发 ，新项目名称为mvc-new

2.2自定义注解@Security，这里需要实现value的数组形式，@Target类型为类或者方法

2.3实现思路：通过扫描类或者方法上的注解，判断是否存在@Security注解，若存在，则比较注解中的username和当前的用户的username进行比较，若一致则拥有权限并放行，执行后续逻辑。若不存在则拦截，并提示“无访问权限”

#### 3.代码实现(项目名称mvc-new)

有访问url：http://localhost:8080/demo/query?username=zhagnsan

无权限url：http://localhost:8080/demo/query?username=wangwu

+ 新增@Security注解

```java
package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

/**
 * 自定义权限注解
 * 入参数组形式
 *
 * @auther: guangjun.ma
 * @date: 2020-4-21 10:39
 * @version: 1.0
 */
@Documented
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Security {
    String[] value() default "";
}

```

+ 在Handle中新增权限列表属性securityUserList

com.lagou.edu.mvcframework.pojo.Handler

```java
package com.lagou.edu.mvcframework.pojo;

import javax.sound.midi.MetaEventListener;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * 封装handler方法相关的信息
 */
public class Handler {

    private Object controller; // method.invoke(obj,)

    private Method method;

    private Pattern pattern; // spring中url是支持正则的

    private Map<String,Integer> paramIndexMapping; // 参数顺序,是为了进行参数绑定，key是参数名，value代表是第几个参数 <name,2>

    private List<String> securityUserList;//权限列表

    public Handler(Object controller, Method method, Pattern pattern) {
        this.controller = controller;
        this.method = method;
        this.pattern = pattern;
        this.paramIndexMapping = new HashMap<>();
    }

    public Object getController() {
        return controller;
    }

    public void setController(Object controller) {
        this.controller = controller;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Pattern getPattern() {
        return pattern;
    }

    public void setPattern(Pattern pattern) {
        this.pattern = pattern;
    }

    public Map<String, Integer> getParamIndexMapping() {
        return paramIndexMapping;
    }

    public void setParamIndexMapping(Map<String, Integer> paramIndexMapping) {
        this.paramIndexMapping = paramIndexMapping;
    }

    public List<String> getSecurityUserList() {
        return securityUserList;
    }

    public void setSecurityUserList(List<String> securityUserList) {
        this.securityUserList = securityUserList;
    }
}

```



+ 在com.lagou.edu.mvcframework.servlet.LgDispatcherServlet中新增权限拦截的处理逻辑,

  将标有@Security的url赋值权限列表handler.setSecurityUserList(securityAnnotationList);

  便于后续逻辑处理。

  com.lagou.edu.mvcframework.servlet.LgDispatcherServlet#initHandlerMapping

  ```java
   /**
               * 若类上标有@Security注解
               */
              List<String> securityAnnotationList = new ArrayList<>();
              if(aClass.isAnnotationPresent(Security.class)) {
                  Security securityAnnotation = aClass.getAnnotation(Security.class);
                  String[] securityAnnotationValue = securityAnnotation.value();
                  securityAnnotationList = Arrays.asList(securityAnnotationValue);
              }
  ```

  ```java
  /**
                   * 若方法上标有@Security注解
                   */
                  if(method.isAnnotationPresent(Security.class)) {
                      Security securityAnnotation = method.getAnnotation(Security.class);
                      String[] securityAnnotationValue = securityAnnotation.value();
                      List<String> securityAnnotationListMethod = Arrays.asList(securityAnnotationValue);
                      securityAnnotationList.addAll(securityAnnotationListMethod);
                  }
  ```

  ```java
  /*
                   权限赋值 @Security注解
                   */
                  handler.setSecurityUserList(securityAnnotationList);
  ```

+ 根据权限列表拦截处理各种url请求

  com.lagou.edu.mvcframework.servlet.LgDispatcherServlet#doPost

  ```java
  
          //从参数中获取name属性值
          Map<String, String[]> parameterMap2 = req.getParameterMap();
          //进行权限拦截
          for(Map.Entry<String,String[]> param: parameterMap2.entrySet()) {
              if("username".equals(param.getKey())){
                  String[] currentUserNames = param.getValue();
                  String currentUserName = currentUserNames[0];
                  if (handler.getSecurityUserList().contains(currentUserName)){
                      continue;
                  }else {
                      resp.getWriter().write(currentUserName + " No permissions!!");
                      return;
                  }
           
              }
          }
  ```

  

+ controller配置权限注解,@Security({"zhangsan","lisi"})

  ```java
  package com.lagou.demo.controller;
  
  import com.lagou.demo.service.IDemoService;
  import com.lagou.edu.mvcframework.annotations.LagouAutowired;
  import com.lagou.edu.mvcframework.annotations.LagouController;
  import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
  import com.lagou.edu.mvcframework.annotations.Security;
  
  import javax.servlet.http.HttpServletRequest;
  import javax.servlet.http.HttpServletResponse;
  import java.io.IOException;
  
  @LagouController
  @LagouRequestMapping("/demo")
  @Security({"zhangsan","lisi"})
  public class DemoController {
  
  
      @LagouAutowired
      private IDemoService demoService;
  
  
      /**
       * URL: /demo/query?name=lisi
       * @param request
       * @param response
       * @param username
       * @return
       */
      @LagouRequestMapping("/query")
      public void query(HttpServletRequest request, HttpServletResponse response,String username) throws IOException {
          demoService.get(username);
          response.getWriter().write(" success!!");
      }
  }
  
  ```

  



#### 4.验证

有访问url：http://localhost:8080/demo/query?username=zhangsan

无权限url：http://localhost:8080/demo/query?username=wangwu

> zhangsan、lisi 在controller中有配置，则有权限
>
> wangwu无配置，则无权限

<img src="note.assets/image-20200421120923177.png" alt="image-20200421120923177" style="zoom:150%;" />



<img src="note.assets/image-20200421120832189.png" alt="image-20200421120832189" style="zoom: 180%;" />



### 作业二：

#### 1.需求

需求：实现登录页面（简易版即可），实现登录验证功能、登录之后跳转到列表页，查询出 tb_resume 表【表数据和课上保持一致】的所有数据（列表不要求分页，在列表右上方有“新增”按钮，每一行后面有“编辑”和“删除”按钮，并实现功能），如果未登录就访问url则跳转到登录页面，用户名和密码固定为admin/admin

技术要求：根据SSM整合的思路，进行SSS整合（Spring+SpringMVC+SpringDataJPA）,登录验证使用SpringMVC拦截器实现

【提交时统一数据库名test，用户名和密码root】



#### 2.需求分析

+ 需要实现简单的jsp管理页面，提供展示、修改、删除等功能。

+ 需要实现权限拦截，未登陆用户需要进行登陆

+ 需要实现登陆页面和功能

  

#### 3.准备数据

+ 数据库sql

````sql
-- SET NAMES utf8mb4;
-- SET FOREIGN_KEY_CHECKS = 0;
-- ----------------------------
-- Table structure for tb_resume
-- ----------------------------
DROP TABLE IF EXISTS `tb_resume`;
CREATE TABLE `tb_resume` (
 `id` bigint(20) NOT NULL AUTO_INCREMENT,
 `address` varchar(255) DEFAULT NULL,
 `name` varchar(255) DEFAULT NULL,
 `phone` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
-- ----------------------------
-- Records of tb_resume
-- ----------------------------
BEGIN;
INSERT INTO `tb_resume` VALUES (1, '北京', '张三', '131000000');
INSERT INTO `tb_resume` VALUES (2, '上海', '李四', '151000000');
INSERT INTO `tb_resume` VALUES (3, '广州', '王五', '153000000');
COMMIT;
SET FOREIGN_KEY_CHECKS = 1;
````

#### 4.代码实现(项目名称sss)

+ 简历管理模块

  com.lagou.edu.controller.ResumeController

  ```java
  package com.lagou.edu.controller;
  
  import com.lagou.edu.pojo.Resume;
  import com.lagou.edu.service.ResumeService;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.stereotype.Controller;
  import org.springframework.web.bind.annotation.RequestMapping;
  import org.springframework.web.servlet.ModelAndView;
  
  import java.util.List;
  
  /**
   * 简历管理
   * @auther: guangjun.ma
   * @date: 2020-4-22 16:23
   * @version: 1.0
   */
  @Controller
  @RequestMapping("/resume")
  public class ResumeController {
  
      @Autowired
      private ResumeService resumeService;
  
      /**
       * 列表展示
       * @return
       * @throws Exception
       * @auther: guangjun.ma
       * @date: 2020-4-22 16:17
       * @version: 1.0
       */
      @RequestMapping("/queryAll")
      public ModelAndView  queryAll() throws Exception {
          List<Resume>  resumeList = resumeService.queryAccountList();
          ModelAndView modelAndView = new ModelAndView();
          modelAndView.addObject("resumeList",resumeList);
          modelAndView.setViewName("resume");
          return modelAndView;
      }
  
      /**
       * 通过id查询
       * @param id
       * @return
       * @throws Exception
       * @auther: guangjun.ma
       * @date: 2020-4-22 16:22
       * @version: 1.0
       */
      @RequestMapping("/queryById")
      public ModelAndView  queryById(long id) throws Exception {
          Resume  resume = resumeService.queryById(id);
          ModelAndView modelAndView = new ModelAndView();
          modelAndView.addObject("resume",resume);
          modelAndView.setViewName("resume_Modify");
          return modelAndView;
      }
  
      /**
       * 更新
       * @param resume
       * @return
       * @throws Exception
       * @auther: guangjun.ma
       * @date: 2020-4-22 16:22
       * @version: 1.0
       */
      @RequestMapping("/modify")
      public String  modify(Resume resume) throws Exception {
          resumeService.saveOrUpdate(resume);
          return  "redirect:queryAll";
      }
  
  
      /**
       * 删除
       * @param id
       * @return
       * @throws Exception、
       * @auther: guangjun.ma
       * @date: 2020-4-22 3:11
       * @version: 1.0
       */
      @RequestMapping("/deleteById")
      public String  deleteById(Long id) throws Exception {
          resumeService.deleteById(id);
          return "redirect:queryAll";
      }
  }
  
  ```

+ 用户登录

  com.lagou.edu.controller.UserController

  ````
  package com.lagou.edu.controller;
  
  import com.lagou.edu.pojo.Resume;
  import org.springframework.stereotype.Controller;
  import org.springframework.web.bind.annotation.RequestMapping;
  import org.springframework.web.servlet.ModelAndView;
  
  import javax.servlet.http.HttpServlet;
  import javax.servlet.http.HttpServletRequest;
  import javax.servlet.http.HttpServletResponse;
  
  /**
   * 用户控制，如登录功能
   * @auther: guangjun.ma
   * @date: 2020-4-22 14:16
   * @version: 1.0
   */
  @Controller
  @RequestMapping("/user")
  public class UserController {
  
      /**
       * 登录页面
       * http://localhost:8080/user/goLogin
       * @return
       * @throws Exception
       * @auther: guangjun.ma
       * @date: 2020-4-22 14:19
       * @version: 1.0
       */
      @RequestMapping("/goLogin")
      public ModelAndView goLogin() throws Exception {
          ModelAndView modelAndView = new ModelAndView();
          modelAndView.setViewName("login");
          return modelAndView;
      }
  
      /**
       * 登录
       * @param username
       * @param password
       * @return
       * @throws Exception
       * @auther: guangjun.ma
       * @date: 2020-4-22 14:24
       * @version: 1.0
       */
      @RequestMapping("/login")
      public String login(HttpServletRequest request, HttpServletResponse response, String username, String password) throws Exception {
          ModelAndView modelAndView = new ModelAndView();
          if ("root".equals(username) && "root".equals(password)){
              request.getSession().setAttribute("root","root");
              return "redirect:/resume/queryAll";//登录成功后，跳转到列表页面
          }else{
              return "login";//登录失败后，跳转到登录页面
          }
      }
  }
  
  ````

  

+ Dao层实现

  ```java
     /**
       * 根据id删除
       * @param id
       * @auther: guangjun.ma
       * @date: 2020-4-22 10:09
       * @version: 1.0
       */
      public void deleteById(Long id);
  
  
      /**
       * 更新数据
       * @param resume
       * @return
       * @auther: guangjun.ma
       * @date: 2020-4-22 10:46
       * @version: 1.0
       */
      public Resume saveAndFlush(Resume resume);
  
      /**
       * 根据id查找
       * @param id
       * @return
       * @auther: guangjun.ma
       * @date: 2020-4-22 11:27
       * @version: 1.0
       */
      public Optional<Resume> findById(Long id);
  ```

  

+ 拦截器

  ```java
  package com.lagou.edu.intecepter;
  
  import org.springframework.web.servlet.HandlerInterceptor;
  import org.springframework.web.servlet.ModelAndView;
  
  import javax.servlet.http.HttpServletRequest;
  import javax.servlet.http.HttpServletResponse;
  import java.util.TreeMap;
  
  /**
   * 登录拦截
   * @auther: guangjun.ma
   * @date: 2020-4-22 16:46
   * @version: 1.0
   */
  public class LoginIntercepter implements HandlerInterceptor {
  
      @Override
      public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
          System.out.println("LoginIntercepter.preHandle begin ..");
          Object loginFlag = request.getSession().getAttribute("root");
          if (null == loginFlag){
              System.out.println("登录失败需要登录验证！");
              request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request,response);
              return false;
          }
          System.out.println("登录成功！");
          return true;
      }
  }
  
  ```

  

+ spring-mvc.xml

  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <beans xmlns="http://www.springframework.org/schema/beans"
         xmlns:context="http://www.springframework.org/schema/context"
         xmlns:mvc="http://www.springframework.org/schema/mvc"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.springframework.org/schema/beans
         http://www.springframework.org/schema/beans/spring-beans.xsd
         http://www.springframework.org/schema/context
         http://www.springframework.org/schema/context/spring-context.xsd
         http://www.springframework.org/schema/mvc
         http://www.springframework.org/schema/mvc/spring-mvc.xsd
  
  ">
  
      <!--扫描controller-->
      <context:component-scan base-package="com.lagou.edu.controller"/>
  
      <!--配置springmvc的视图解析器-->
      <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
          <property name="prefix" value="/WEB-INF/jsp/"/>
          <property name="suffix" value=".jsp"/>
      </bean>
  
      <mvc:resources location="/WEB-INF/JS/" mapping="/JS/**"/>
      <mvc:resources location="/WEB-INF/CSS/" mapping="/CSS/**"/>
      <mvc:resources location="/WEB-INF/Images/" mapping="/Images/**"/>
  
  
      <!--配置springmvc注解驱动，自动注册合适的组件handlerMapping和handlerAdapter-->
      <mvc:annotation-driven/>
  
      <mvc:interceptors>
          <mvc:interceptor>
              <mvc:mapping path="/**"/>
              <mvc:exclude-mapping path="/user/**"/>
              <bean class="com.lagou.edu.intecepter.LoginIntercepter"/>
          </mvc:interceptor>
      </mvc:interceptors>
  
  
  </beans>
  ```

  



#### 5.访问链接

http://localhost:8080/resume/queryAll

#### 6.效果展示

http://localhost:8080/resume/queryAll

+ 列表

<img src="note.assets/image-20200422160411269.png" alt="image-20200422160411269" style="zoom:150%;" />

+ 更新

<img src="note.assets/image-20200422160849193.png" alt="image-20200422160849193"  />

+ 登陆

<img src="note.assets/image-20200422160803829.png" alt="image-20200422160803829"  />





--

end

