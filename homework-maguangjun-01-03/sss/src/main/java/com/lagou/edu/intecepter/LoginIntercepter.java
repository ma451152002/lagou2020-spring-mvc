package com.lagou.edu.intecepter;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.TreeMap;

/**
 * 登录拦截
 * @auther: guangjun.ma
 * @date: 2020-4-22 16:46
 * @version: 1.0
 */
public class LoginIntercepter implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("LoginIntercepter.preHandle begin ..");
        Object loginFlag = request.getSession().getAttribute("root");
        if (null == loginFlag){
            System.out.println("登录失败需要登录验证！");
            request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request,response);
            return false;
        }
        System.out.println("登录成功！");
        return true;
    }
}
